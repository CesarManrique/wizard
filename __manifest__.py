# -*- coding: utf-8 -*-
{
    'name': "Wizzard Picking stocks",

    'summary': """Wizzard Picking stocks""",

    'description': """
        Some descripcion
    """,

    'author': "Cesar",
    'website': "http://www.cesar.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/picking.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
}
