# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class Wizard(models.TransientModel):
    _name = 'wizzard.wizard'

    def _default_stock_pickings(self):
        stock_pickings = self.env['stock.picking'].browse(self._context.get('active_ids'))
        if len(stock_pickings) < 2:
            raise exceptions.ValidationError("Should select more than one stock picking")
        return stock_pickings

    stock_picking_ids = fields.Many2many('stock.picking', string="Stock pickings", required=True, default=_default_stock_pickings)
